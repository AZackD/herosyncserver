﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HeroSyncServer.DatabaseHandling
{
    /**
     * This is a C# object that mirrors how the database (theoretically) would store category information. 
     */

    public class DBConditionCategoryRepresentation
    {
        public int category_unique_id;
        public string field;
        public string stringValue;
        public int minValue;
        public int maxValue;
        public int id_val;
        public bool usingId;
    }
}
