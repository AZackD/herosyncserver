﻿using MySql.Data;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using DarkRift.Server;
using HeroSyncCommon;
using HeroSyncServer.DatabaseHandling;
using HeroSyncServer.GameLogic;

public class DBConnect
{
    private MySqlConnection connection;
    private string server;
    private string database;
    private string uid;
    private string password;
    public Logger logger;

    //Constructor
    public DBConnect(Logger logger)
    {
        Initialize(logger);
    }

    //Initialize values
    private void Initialize(Logger logger)
    {
        server = "localhost";
        database = "herosyncdemo";
        uid = "admin";
        password = "password";
        string connectionString;
        connectionString = "SERVER=" + server + ";" + "DATABASE=" +
                           database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

        connection = new MySqlConnection(connectionString);
        this.logger = logger;
    }

    //open connection to database
    private bool OpenConnection()
    {
        try
        {
            connection.Open();
            return true;
        }
        catch (MySqlException ex)
        {
            //When handling errors, you can your application's response based 
            //on the error number.
            //The two most common error numbers when connecting are as follows:
            //0: Cannot connect to server.
            //1045: Invalid user name and/or password.
            switch (ex.Number)
            {
                case 0:
                    logger.Error("Cannot connect to server.  Contact administrator");
                    break;

                case 1045:
                    logger.Error("Invalid username/password, please try again");
                    break;
            }
            return false;
        }
    }

    //Close connection
    private bool CloseConnection()
    {
        try
        {
            connection.Close();
            return true;
        }
        catch (MySqlException ex)
        {
            logger.Error(ex.Message);
            return false;
        }
    }

    //Insert statement
    public void Insert()
    {
    }

    //Update statement
    public void Update()
    {
    }

    //Delete statement
    public void Delete()
    {
    }

    public DatabaseDeckRepresentation SelectDeck(short deck_id)
    {
        DatabaseDeckRepresentation ddr = new DatabaseDeckRepresentation();
        string query = String.Format("SELECT * FROM herosyncdemo.decks WHERE id={0}", deck_id);
        if (this.OpenConnection())
        {
            MySqlCommand command = new MySqlCommand(query, connection);
            MySqlDataReader dataReader = command.ExecuteReader();
            if (dataReader.Read())
            {
                ddr.main_1 = Convert.ToInt16(dataReader["main_1"].ToString());
                ddr.main_2 = Convert.ToInt16(dataReader["main_2"].ToString());
                ddr.main_3 = Convert.ToInt16(dataReader["main_3"].ToString());
                ddr.main_4 = Convert.ToInt16(dataReader["main_4"].ToString());
                ddr.main_5 = Convert.ToInt16(dataReader["main_5"].ToString());
                ddr.main_6 = Convert.ToInt16(dataReader["main_6"].ToString());
                ddr.main_7 = Convert.ToInt16(dataReader["main_7"].ToString());
                ddr.main_8 = Convert.ToInt16(dataReader["main_8"].ToString());
                ddr.main_9 = Convert.ToInt16(dataReader["main_9"].ToString());
                ddr.main_10 = Convert.ToInt16(dataReader["main_10"].ToString());
                ddr.main_11 = Convert.ToInt16(dataReader["main_11"].ToString());
                ddr.main_12 = Convert.ToInt16(dataReader["main_12"].ToString());
                ddr.main_13 = Convert.ToInt16(dataReader["main_13"].ToString());
                ddr.main_14 = Convert.ToInt16(dataReader["main_14"].ToString());
                ddr.main_15 = Convert.ToInt16(dataReader["main_15"].ToString());
                ddr.main_16 = Convert.ToInt16(dataReader["main_16"].ToString());
                ddr.main_17 = Convert.ToInt16(dataReader["main_17"].ToString());
                ddr.main_18 = Convert.ToInt16(dataReader["main_18"].ToString());
                ddr.main_19 = Convert.ToInt16(dataReader["main_19"].ToString());
                ddr.main_20 = Convert.ToInt16(dataReader["main_20"].ToString());
                ddr.main_21 = Convert.ToInt16(dataReader["main_21"].ToString());
                ddr.main_22 = Convert.ToInt16(dataReader["main_22"].ToString());
                ddr.main_23 = Convert.ToInt16(dataReader["main_23"].ToString());
                ddr.main_24 = Convert.ToInt16(dataReader["main_24"].ToString());
                ddr.main_25 = Convert.ToInt16(dataReader["main_25"].ToString());
                ddr.main_26 = Convert.ToInt16(dataReader["main_26"].ToString());
                ddr.main_27 = Convert.ToInt16(dataReader["main_27"].ToString());
                ddr.main_28 = Convert.ToInt16(dataReader["main_28"].ToString());
                ddr.main_29 = Convert.ToInt16(dataReader["main_29"].ToString());
                ddr.main_30 = Convert.ToInt16(dataReader["main_30"].ToString());
                ddr.main_31 = Convert.ToInt16(dataReader["main_31"].ToString());
                ddr.main_32 = Convert.ToInt16(dataReader["main_32"].ToString());
                ddr.main_33 = Convert.ToInt16(dataReader["main_33"].ToString());
                ddr.main_34 = Convert.ToInt16(dataReader["main_34"].ToString());
                ddr.main_35 = Convert.ToInt16(dataReader["main_35"].ToString());
                ddr.main_36 = Convert.ToInt16(dataReader["main_36"].ToString());
                ddr.main_37 = Convert.ToInt16(dataReader["main_37"].ToString());
                ddr.main_38 = Convert.ToInt16(dataReader["main_38"].ToString());
                ddr.main_39 = Convert.ToInt16(dataReader["main_39"].ToString());
                ddr.main_40 = Convert.ToInt16(dataReader["main_40"].ToString());
                ddr.main_41 = Convert.ToInt16(dataReader["main_41"].ToString());
                ddr.main_42 = Convert.ToInt16(dataReader["main_42"].ToString());
                ddr.main_43 = Convert.ToInt16(dataReader["main_43"].ToString());
                ddr.main_44 = Convert.ToInt16(dataReader["main_44"].ToString());
                ddr.main_45 = Convert.ToInt16(dataReader["main_45"].ToString());
                ddr.deck_id = deck_id;
                ddr.deckname = dataReader["deck_name"].ToString();
                ddr.hero_id = Convert.ToInt16(dataReader["hero"].ToString());
            }
        }
        CloseConnection();
        return ddr;
    }

    public List<CardData> SelectCards()
    {
        return SelectCards("SELECT * FROM herosyncdemo.cards");
    }



    public List<CardData> SelectCardsFromDeck(short id) 
    {
        string query = String.Format("SELECT cards.* FROM cards INNER JOIN decks ON cards.id = decks.main_1 or " +
                                     "cards.id = decks.main_2 or " +
                                     "cards.id = decks.main_3 or " +
                                     "cards.id = decks.main_4 or " +
                                     "cards.id = decks.main_5 or " +
                                     "cards.id = decks.main_6 or " +
                                     "cards.id = decks.main_7 or " +
                                     "cards.id = decks.main_8 or " +
                                     "cards.id = decks.main_9 or " +
                                     "cards.id = decks.main_10 or " +
                                     "cards.id = decks.main_11 or " +
                                     "cards.id = decks.main_12 or " +
                                     "cards.id = decks.main_13 or " +
                                     "cards.id = decks.main_14 or " +
                                     "cards.id = decks.main_15 or " +
                                     "cards.id = decks.main_16 or " +
                                     "cards.id = decks.main_17 or " +
                                     "cards.id = decks.main_18 or " +
                                     "cards.id = decks.main_19 or " +
                                     "cards.id = decks.main_20 or " +
                                     "cards.id = decks.main_21 or " +
                                     "cards.id = decks.main_22 or " +
                                     "cards.id = decks.main_23 or " +
                                     "cards.id = decks.main_24 or " +
                                     "cards.id = decks.main_25 or " +
                                     "cards.id = decks.main_26 or " +
                                     "cards.id = decks.main_27 or " +
                                     "cards.id = decks.main_28 or " +
                                     "cards.id = decks.main_29 or " +
                                     "cards.id = decks.main_30 or " +
                                     "cards.id = decks.main_31 or " +
                                     "cards.id = decks.main_32 or " +
                                     "cards.id = decks.main_33 or " +
                                     "cards.id = decks.main_34 or " +
                                     "cards.id = decks.main_35 or " +
                                     "cards.id = decks.main_36 or " +
                                     "cards.id = decks.main_37 or " +
                                     "cards.id = decks.main_38 or " +
                                     "cards.id = decks.main_39 or " +
                                     "cards.id = decks.main_40 or " +
                                     "cards.id = decks.main_41 or " +
                                     "cards.id = decks.main_42 or " +
                                     "cards.id = decks.main_43 or " +
                                     "cards.id = decks.main_44 or " +
                                     "cards.id = decks.main_45 where decks.id = {0}", id);
        return SelectCards(query);
    }


    public List<CardData> SelectCards(string query)
    {
        List<CardData> results = new List<CardData>();
        if (this.OpenConnection())
        {
            MySqlCommand command = new MySqlCommand(query, connection);
            MySqlDataReader dataReader = command.ExecuteReader();
            while (dataReader.Read())
            {
                CardData cardData = new CardData();
                cardData.ID = Convert.ToInt16(dataReader["id"].ToString());
                cardData.level = Convert.ToInt16(dataReader["level"].ToString());
                cardData.strength = Convert.ToInt16(dataReader["power"].ToString());
                cardData.heroID = Convert.ToInt16(dataReader["hero_id"].ToString());
                CardType type;
                if(Enum.TryParse(dataReader["card_type"].ToString(), out type)) {
                    cardData.type = type;
                }
                else
                {
                    logger.Error("Tried to get a card type from the type field, but it didn't work out.");
                }
                cardData.englishName = dataReader["name"].ToString();
                cardData.englishDescription = dataReader["effect"].ToString();
                results.Add(cardData);
            }
        }

        CloseConnection();
        return results;
    }

    public void GetGameplayData(CardServerData baseData)
    {
        if (baseData.baseData == null)
        {
            logger.Error("Can't download gameplay data for card with missing base data");
            return;
        }
        // string sqlQuery = 


    }
}