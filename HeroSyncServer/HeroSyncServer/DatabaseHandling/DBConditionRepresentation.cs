﻿using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon;
using HeroSyncCommon.CoreDataTypes;
using HeroSyncServer.GameLogic.EventProcessing;

namespace HeroSyncServer.DatabaseHandling
{
    /**
     * A C# representation of how the SQL DB would store a specific condition. This could be used for activation conditions or targeting conditions.
     */
    public class DBConditionRepresentation
    {
        public ActionData.ActionTypeEnum effect_type_id;
        public Timing timing;

        public string activation_condition_name;
        public bool specifyTargetCategories;
        public bool specifySourceCategories;
        public bool isChoice;
        public List<DBConditionCategoryRepresentation> targetCategories;
        public List<DBConditionCategoryRepresentation> sourceCategories;
    }
}
