﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DarkRift;
using DarkRift.Server;
using HeroSyncCommon;
using HeroSyncCommon.GameLogic;
using HeroSyncCommon.NetworkActions;
using HeroSyncServer.DatabaseHandling;
using HeroSyncServer.GameLogic;
using HeroSyncServer.GameLogic.EventProcessing;
using HeroSyncServer.GameLogic.EventProcessing.Events;
using HeroSyncServer.GameLogic.EventProcessing.Processors;
using HeroSyncServer.GameLogic.InputHandlers;
using HeroSyncServer.GameLogic.Targeting;
using HeroSyncServer.SinglePlayerShortcuts;
using HeroSyncServer.Utilities;

namespace HeroSyncServer
{
    public class GameRoom
    {
        public DBConnect dbConnect;

        public IClient BluePlayerClient;
        public IClient OrangePlayerClient;

        public PlayerData bluePlayer;
        public PlayerData orangePlayer;

        public PlayerField gameField;

        public PlayerData currentPlayer;
        public PlayerData leadingPlayer;

        private bool isAvailableForFeedback;

        public int turnCount;
        public Dictionary<CardData, CardServerData> cardStatusDictionary;

        private EventStack eventStack;
        private EventProcessorFactory eventProcessorFactory;
        private EventProcessorList eventProcessorList;
        private InputHandler inputHandler;

        private bool blueReady = false;
        private bool orangeReady = false;

        private bool isSinglePlayer = false;
        private bool isStarted = false;
        public bool testMode = false;

        public HeroSyncGameManager gameManager;
        public GameRoom()
        {
            cardStatusDictionary = new Dictionary<CardData, CardServerData>();
        }

        public void Start(HeroSyncGameManager gm)
        {
            if (isStarted)
            {
                return;
            }
            gameManager = gm;
            isStarted = true;
            dbConnect.logger.Info("Starting Game Room");
            cardStatusDictionary = new Dictionary<CardData, CardServerData>();
            turnCount = 1;
            bluePlayer.influence = 400;
            bluePlayer.playerColor = PlayerData.PlayerColor.BLUE;
            InitializeHero(bluePlayer, 1);
            InitializeDeck(bluePlayer, 7);
            InitializeHand(bluePlayer);

            orangePlayer.influence = 400;
            orangePlayer.playerColor = PlayerData.PlayerColor.ORANGE;
            InitializeHero(orangePlayer, 2);
            InitializeDeck(orangePlayer, 8);
            InitializeHand(orangePlayer);

            SetupGameRoomGlobalReqs();

            turnCount = 1;

            BluePlayerClient.MessageReceived += SendSetupInfo;
            OrangePlayerClient.MessageReceived += SendSetupInfo;

            Random randInt = new Random();

            leadingPlayer = randInt.Next() % 2 == 1 ? orangePlayer : bluePlayer;
            isAvailableForFeedback = true;
            inputHandler = new InputHandler();

        }

        private void SendSetupInfo(object sender, MessageReceivedEventArgs e)
        {
            dbConnect.logger.Info("Receiving setup info");
            if (e.Tag == (short) NetworkStateTags.ClientMessageTags.GAME_ROOM_ENTERED)
            {
                blueReady = blueReady || e.Client == BluePlayerClient;
                orangeReady = orangeReady || e.Client == OrangePlayerClient;
            }

            if (blueReady && orangeReady)
            {
                dbConnect.logger.Info("Sending info to other rooms");
                this.SendMessage(bluePlayer, NetworkStateTags.ServerMessageTags.ADD_BLUE_TO_ROOM, SendMode.Reliable);
                this.SendMessage(orangePlayer, NetworkStateTags.ServerMessageTags.ADD_ORANGE_TO_ROOM, SendMode.Reliable);
                SetCurrentPlayer(leadingPlayer);
                BluePlayerClient.MessageReceived += HandleBluePlayerInput;
                OrangePlayerClient.MessageReceived += HandleOrangePlayerInput;
                BluePlayerClient.MessageReceived -= SendSetupInfo;
                OrangePlayerClient.MessageReceived -= SendSetupInfo;
            }
        }

        internal PlayerData GetPlayer(PlayerData.PlayerColor playerColor)
        {
            return playerColor == PlayerData.PlayerColor.BLUE ? bluePlayer : orangePlayer;
        }

        public void StartSingle(HeroSyncGameManager gm)
        {
            if (isStarted)
            {
                return;
            }
            isStarted = true;
            gameManager = gm;
            dbConnect.logger.Log("Starting single...", LogType.Info);
            cardStatusDictionary = new Dictionary<CardData, CardServerData>();
            turnCount = 1;

            bluePlayer.influence = 1;
            bluePlayer.playerColor = PlayerData.PlayerColor.BLUE;
            InitializeHero(bluePlayer, 1);
            InitializeDeck(bluePlayer, 7);
            InitializeHand(bluePlayer);

            orangePlayer.influence = 1;
            orangePlayer.playerColor = PlayerData.PlayerColor.ORANGE;
            InitializeHero(orangePlayer, 2);
            InitializeDeck(orangePlayer, 8);
            InitializeHand(orangePlayer);
            SetupGameRoomGlobalReqs();

            SendMessage(bluePlayer, NetworkStateTags.ServerMessageTags.ADD_BLUE_TO_ROOM, SendMode.Reliable);
            SendMessage(orangePlayer, NetworkStateTags.ServerMessageTags.ADD_ORANGE_TO_ROOM, SendMode.Reliable);

            leadingPlayer = bluePlayer;
            SetCurrentPlayer(leadingPlayer);
            isAvailableForFeedback = true;
            inputHandler = new InputHandler();
            BluePlayerClient.MessageReceived += HandleBluePlayerInput;
            SinglePlayerDebugCommandReceiver debugCommandReceiver = new SinglePlayerDebugCommandReceiver(this);
            BluePlayerClient.MessageReceived += debugCommandReceiver.HandleDebugInput;
        }

        public PlayerData EndTurn()
        {
            Console.WriteLine("Ending {0} turn", currentPlayer.playerColor);
            PlayerData oldCurrentPlayer = currentPlayer;
            PlayerData newCurrentPlayer = currentPlayer == bluePlayer ? orangePlayer : bluePlayer;

            turnCount++;
            int levelCapTurn = 9;

            if (leadingPlayer == newCurrentPlayer && turnCount < 9)
            {
                turnCount++;
                // TODO: Characters level up needs more than this; we also need to ask what perk they're taking. 
                // Some of that (and this) might be better off as part of the EndTurnAction.
                bluePlayer.playerHero.level++;
                orangePlayer.playerHero.level++;
                SendMessage(bluePlayer, NetworkStateTags.ServerMessageTags.UPDATE_PLAYER_DATA, SendMode.Reliable);
                SendMessage(orangePlayer, NetworkStateTags.ServerMessageTags.UPDATE_PLAYER_DATA, SendMode.Reliable);
            }
            SetCurrentPlayer(newCurrentPlayer);
            CardPermissionCalculator.ResetAllTransientFlags(this);
            CardPermissionCalculator.UpdateAllCardPermissions(this);
            CardSet cardSet = new CardSet();
            cardSet.cardSet = cardStatusDictionary.Keys.ToList();
            cardSet.cardCount = cardSet.cardSet.Count;
            SendMessage(cardSet, NetworkStateTags.ServerMessageTags.UPDATE_CARD_DATA, SendMode.Reliable);
            return newCurrentPlayer;
        }


        public void SetCurrentPlayer(PlayerData player)
        {
            if (player != currentPlayer)
            {
                // string formattedCurrentPlayerString = String.Format("Setting player {0} to current player", player.playerColor);
                // dbConnect.logger.Info(formattedCurrentPlayerString);
                SendMessage(player, NetworkStateTags.ServerMessageTags.SET_CURRENT_PLAYER, SendMode.Reliable);
                currentPlayer = player;
            }
        }

        public void SetupGameRoomGlobalReqs()
        {
            gameField = new PlayerField(orangePlayer.playerDeck, bluePlayer.playerDeck);
            eventProcessorFactory = new EventProcessorFactory(this);
            eventProcessorList = new EventProcessorList(this);

            eventStack = new EventStack(eventProcessorList, this);
        }

        // TODO: Replace contents with asking the db for info.
        private void InitializeHero(PlayerData player, int hero_id)
        {
            CharacterData charData = new CharacterData();
            charData.level = 1;
            charData.characterId = (short) hero_id;
            charData.characterName = "Null";
            player.playerHero = charData;
        }

        private void InitializeDeck(PlayerData player, short deck_id)
        {
            Deck playerDeck = new Deck();
            playerDeck.id = deck_id;
            List<CardData> allCards = dbConnect.SelectCardsFromDeck(deck_id);
            DatabaseDeckRepresentation deckRepr = dbConnect.SelectDeck(deck_id);
            deckRepr.ConstructDeck(allCards, playerDeck, player.playerColor==PlayerData.PlayerColor.BLUE);
            
            string formattedLogString = String.Format("Found {0} cards in the database", allCards.Count);
            dbConnect.logger.Log(formattedLogString, LogType.Info);
            player.playerDeck = playerDeck;
            foreach (CardData card in playerDeck.cards)
            {
                CardServerData status = new CardServerData(card, player);
                status.location = CardLocation.Deck;
                status.currentZone = PlayerField.Zone.BlueDeckArea;
                if (player.playerColor != PlayerData.PlayerColor.BLUE)
                {
                    status.currentZone = PlayerField.Zone.OrangeDeckArea;
                }

                status.UpdateLevelLegality();
                cardStatusDictionary.Add(card, status);
            }
            dbConnect.logger.Log("All cards have been given a card status and assigned to a dictionary.", LogType.Info);
            Random rand = new Random();
            player.playerDeck.cards = player.playerDeck.cards.OrderBy(x => rand.Next()).ToList();
        }

        // TODO: This eventually needs to ask the player what 1 card they want to start with. For now it just does all the cards randomly.
        private void InitializeHand(PlayerData player)
        {
            List<CardData> cardsInDeck = player.playerDeck.cards;
            player.cardsInHand =  new List<CardData>();

            // This is a fill-in for the player getting to select a card for their starting hand.
            CardData tempSelectedCard = cardsInDeck.Find(x => x.type == CardType.Follower && x.level == 1);
            this.AddCardToHand(tempSelectedCard, player);

            for (int i = 0; i < 4; i++)
            {
                int cardIndex = player.playerDeck.cards.Count - (i + 1);
                CardData card = player.playerDeck.cards[cardIndex];
                this.AddCardToHand(card, player);
            }
            player.playerDeck.cards = cardsInDeck;
        }

        public void AddBluePlayer(IClient playerClient, PlayerData player)
        {
            bluePlayer = player;
            BluePlayerClient = playerClient;
        }

        public void AddOrangePlayer(IClient playerClient, PlayerData player)
        {
            orangePlayer = player;
            OrangePlayerClient = playerClient;
        }

        private void HandleBluePlayerInput(object sender, MessageReceivedEventArgs e)
        {
            if (bluePlayer.Equals(currentPlayer) && isAvailableForFeedback)
            {
                HandlePlayerInput(sender, e, bluePlayer);
            }
        }

        private void HandleOrangePlayerInput(object sender, MessageReceivedEventArgs e)
        {
            if (orangePlayer.Equals(currentPlayer) && isAvailableForFeedback)
            {
                HandlePlayerInput(sender, e, orangePlayer);
            }
        }

        private void HandlePlayerInput(object sender, MessageReceivedEventArgs e, PlayerData player)
        {
            string inputLog = String.Format("Received message from client. Has tag {0}",
                (NetworkStateTags.ClientMessageTags) e.Tag);
            dbConnect.logger.Log(inputLog, LogType.Info);
            isAvailableForFeedback = false;
            inputHandler.HandleMessage((NetworkStateTags.ClientMessageTags)e.Tag, e.GetMessage(), this);
            isAvailableForFeedback = true;
        }

        public void AddEventToStack(Event e)
        {
            eventStack.AddToStack(e);
        }

        public void AddEventToStack((Event, Targeter) eventTargeterTuple)
        {
            AddEventToStack(eventTargeterTuple.Item1, eventTargeterTuple.Item2);
        }

        public void AddEventToStack(Event e, Targeter t)
        {
            if (e.GetTarget() == null && !(t.GetType() != typeof(NoTargeter)))
            {
                e.SetTarget(
                    t.GetTargets(this)[0]);
            }
            this.AddEventToStack(e);
        }

        public void AddEventProcessor(EventProcessor ep)
        {
            eventProcessorList.AddProcessor(ep);
        }

        public void PlayerLoss(PlayerData lossPlayer)
        {
            PlayerData otherPlayer = lossPlayer == orangePlayer ? bluePlayer : orangePlayer;

            bool legalLoss = false;
            bool lossPlayerLost = false;
            bool otherPlayerLost = false;

            lossPlayerLost = lossPlayerLost || lossPlayer.influence < 0;
            lossPlayerLost = lossPlayerLost || lossPlayer.playerDeck.cards.Count <= 0;
            otherPlayerLost = otherPlayerLost || otherPlayer.influence < 0;
            otherPlayerLost = otherPlayerLost || otherPlayer.playerDeck.cards.Count <= 0;

            legalLoss = lossPlayerLost || otherPlayerLost;

            if (lossPlayerLost && otherPlayerLost) // If they lose at the same time, need to set loss player to biggest loser in the influence case.
            {
                if (lossPlayer.influence < 0 || otherPlayer.influence < 0)
                {
                    lossPlayer = lossPlayer.influence < otherPlayer.influence ? lossPlayer : otherPlayer;
                    otherPlayer = lossPlayer == orangePlayer ? bluePlayer : orangePlayer;
                }
            }
            GameOver gameOver = new GameOver();
            gameOver.winnerName = otherPlayer.username;
            gameOver.winningColor = otherPlayer.playerColor;
            SendMessage(gameOver, NetworkStateTags.ServerMessageTags.GAME_OVER, SendMode.Reliable);

        }

        internal void SendMessage(IDarkRiftSerializable serializableObject, NetworkStateTags.ServerMessageTags tag, SendMode sendMode)
        {
            if (testMode)
            {
                // Do nothing
                return;
            }

            using (Message message = Message.Create((ushort)(tag), serializableObject))
            {
                if (BluePlayerClient != null)
                {
                    BluePlayerClient.SendMessage(message, sendMode);
                }

                if (OrangePlayerClient != null)
                {
                    OrangePlayerClient.SendMessage(message, sendMode);
                }
            }
        }

        internal PlayerField.Zone GetFallenArea(PlayerData.PlayerColor color)
        {
            return color == PlayerData.PlayerColor.BLUE
                ? PlayerField.Zone.BlueFallenArea
                : PlayerField.Zone.OrangeFallenArea;
        }

        public bool AddCardToZone(CardData card, PlayerField.Zone zone)
        {
            if(gameField.CanAddCardsToZone(card, zone))
            {
                gameField.AddCardsToZone(card, zone);
                CardLocation loc = ZoneConverter.ToLocation(zone);
                if (cardStatusDictionary.ContainsKey(card))
                {
                    cardStatusDictionary[card].location = loc;
                    cardStatusDictionary[card].currentZone = zone;
                }
                else
                {
                    Console.Error.WriteLine("Received a card with no card status data present in the dictionary.");
                    return true;
                }

                CardPermissionCalculator.SetCardPermissions(card, cardStatusDictionary[card]);
                return true;
            }
            return false;
        }

        internal CardServerData GetCardServerData(CardData card)
        {
            if (!cardStatusDictionary.ContainsKey(card))
            {
                Console.Error.WriteLine("Server is missing data for card {0} with ID {1}", card.englishName, card.ID);
                return null;
            }

            return cardStatusDictionary[card];
        }
    }
}
