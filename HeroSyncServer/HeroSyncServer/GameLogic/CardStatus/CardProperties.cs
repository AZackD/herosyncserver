﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HeroSyncServer.GameLogic
{
    public static class CardProperties
    {
        public static List<Property> GetTransientFlags()
        {
            return new List<Property>()
            {
                Property.HAS_ATTACKED
            };

        }

    }
    public enum Property
    {
        HAS_ATTACKED,
        HIDDEN
    }

}
