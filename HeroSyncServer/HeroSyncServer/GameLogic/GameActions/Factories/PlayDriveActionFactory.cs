﻿using HeroSyncCommon.CoreDataTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace HeroSyncServer.GameLogic.GameActions.Factories
{
    public class PlayDriveActionFactory : ActionFactoryInterface
    {
        private string desc = "Play Drive";

        public GameAction GenerateAction(GameRoom gr)
        {
            ActionData actionData = new ActionData();
            actionData.type = this.GetActionType();
            actionData.id = Guid.NewGuid();
            actionData.desc = desc;

            PlayDriveAction action = new PlayDriveAction(actionData, gr);
            return action;
        }

        public ActionData.ActionTypeEnum GetActionType()
        {
            return ActionData.ActionTypeEnum.PlayDrive;
        }
    }
}
