﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using HeroSyncCommon;
using HeroSyncCommon.CoreDataTypes;
using HeroSyncServer.GameLogic.GameActions.Factories;
using HeroSyncServer.Utilities;

namespace HeroSyncServer.GameLogic.GameActions
{
    public class ActionFactory
    {
        private Dictionary<ActionData.ActionTypeEnum, ActionFactoryInterface> typeActionFactoryDictionary;
        private GameRoom game;

        public GameAction CreateAction(ActionData.ActionTypeEnum type)
        {
            if (!typeActionFactoryDictionary.ContainsKey(type))
            {
                Console.Error.Write("Passed type {0} to the gameAction factory, but the factory has no component to build that gameAction type.", type);
                return null;
            }

            ActionFactoryInterface relevantFactory = typeActionFactoryDictionary[type];
            GameAction generatedGameAction = relevantFactory.GenerateAction(game);
            return generatedGameAction;
        }

        public ActionFactory(GameRoom gr)
        {
            if (gr == null)
            {
                throw new NoNullAllowedException("ActionFactory was passed a null gameroom. This is illegal; as actions need to know what game they're acting on.");
            }

            game = gr;
            typeActionFactoryDictionary = new Dictionary<ActionData.ActionTypeEnum, ActionFactoryInterface>();

            var type = typeof(ActionFactoryInterface);
            var types = GetImplementationUtility.GetImplementations<ActionFactoryInterface>();

            foreach (var actionFactoryType in types)
            {
                if (!actionFactoryType.IsInterface)
                {
                    ActionFactoryInterface afi = (ActionFactoryInterface) Activator.CreateInstance(actionFactoryType);
                    ActionData.ActionTypeEnum actionType = afi.GetActionType();
                    if (typeActionFactoryDictionary.ContainsKey(actionType))
                    {
                        Console.Error.WriteLine("GameAction Factory had multiple factories for gameAction type {0}: Using the first of the set found.", actionType);
                        continue;
                    }
                    typeActionFactoryDictionary.Add(actionType, afi);
                }
            }
        }
    }
}
