﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using HeroSyncCommon;
using HeroSyncCommon.CoreDataTypes;

namespace HeroSyncServer.GameLogic.GameActions.Factories
{
    public class SummonLogicActionFactory : ActionFactoryInterface
    {
        private readonly string summonActionDesc = "Summon Follower";

        public GameAction GenerateAction(GameRoom gr)
        {
            if (gr == null)
            {
                throw new NoNullAllowedException("Summon GameAction Factory passed a null game room; summon logicGameAction needs to know what game to effect.");
            }

            ActionData actionData = new ActionData();
            actionData.id = Guid.NewGuid();
            actionData.type = GetActionType();
            actionData.desc = summonActionDesc;
            SummonLogicGameAction logicGameAction = new SummonLogicGameAction(actionData, gr);
            return logicGameAction;
        }

        public ActionData.ActionTypeEnum GetActionType()
        {
            return ActionData.ActionTypeEnum.SummonFollower;
        }
    }
}
