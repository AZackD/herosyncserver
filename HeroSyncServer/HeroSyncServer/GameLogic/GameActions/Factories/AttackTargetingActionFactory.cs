﻿using HeroSyncCommon.CoreDataTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace HeroSyncServer.GameLogic.GameActions.Factories
{
    public class AttackTargetingActionFactory : ActionFactoryInterface
    {
        private ActionData.ActionTypeEnum attackCardTarget = ActionData.ActionTypeEnum.TargetFollower;
        private static string actionTargetDesc = "SELECTING A FOLLOWER TO ATTACK (Frontend has already done this, this action is the game's response.)";


        public GameAction GenerateAction(GameRoom gr)
        {
            ActionData attackTargetData = new ActionData();
            attackTargetData.id = Guid.NewGuid();
            attackTargetData.type = attackCardTarget;
            attackTargetData.desc = actionTargetDesc;
            AttackTargetingAction ata = new AttackTargetingAction(attackTargetData, gr);
            return ata;
        }

        public ActionData.ActionTypeEnum GetActionType()
        {
            return attackCardTarget;
        }
    }
}
