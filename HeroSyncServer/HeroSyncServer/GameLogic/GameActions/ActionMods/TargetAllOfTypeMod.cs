﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HeroSyncCommon;
using HeroSyncCommon.CoreDataTypes;
using HeroSyncServer.GameLogic.EventProcessing;
using HeroSyncServer.GameLogic.EventProcessing.Processors.EventEvaluators;

namespace HeroSyncServer.GameLogic.GameActions.ActionMods
{
    public class TargetAllOfTypeMod : GameActionMod
    {
        private List<ObjectCondition> conditions;

        public TargetAllOfTypeMod(GameAction b, List<ObjectCondition> conditions) : base(null, null, b)
        {
            this.conditions = conditions;
            if (conditions == null)
            {
                this.conditions = new List<ObjectCondition>();
            }
        }

        public override void TakeAction(Event e)
        {
            List<CardData> cardsInPlay = gameRoom.gameField.cardsInPlay.Values.ToList();
            foreach (CardData card in cardsInPlay)
            {
                bool shouldBeActedUpon = true;
                foreach (ObjectCondition condition in conditions)
                {
                    shouldBeActedUpon = shouldBeActedUpon && condition.MeetsCondition(card);
                }

                if (shouldBeActedUpon)
                {
                    Event copiedEvent = Event.CopyEvent(e);
                    e.SetTarget(card);
                    BaseGameAction.TakeAction(e);
                }
            }
        }
    }
}
