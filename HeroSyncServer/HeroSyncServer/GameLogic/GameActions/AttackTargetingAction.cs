﻿using HeroSyncServer.GameLogic.EventProcessing;
using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon.CoreDataTypes;
using HeroSyncServer.GameLogic.EventProcessing.Events;

namespace HeroSyncServer.GameLogic.GameActions
{
    /**
     * The attack target event contains all of the information required for the attack to go through, but we need to finish
     * processing it before we can start the actual attack, so all it does is create an attack event and attach it to the original event.
     * Once the AttackTargetEvent has been finished processing, the caller of the attack target event needs to call the actual attack event,
     * assuming that the attack event is still relevant.
     */

    public class AttackTargetingAction : GameAction
    {
        public override void TakeAction(Event e)
        {
            if (e.GetType() != typeof(AttackTargetEvent))
            {
                Console.Error.WriteLine("Tried to pass the Attack Targeting Action an incorrect event type.");
                return;
            }
            AttackTargetEvent ate = (AttackTargetEvent) e;
            AttackEvent attackEvent = new AttackEvent(ate.GetSource(), ate);
            ate.resultingEvent = attackEvent;
        }

        public AttackTargetingAction(ActionData baseData, GameRoom gr) : base(baseData, gr) { }
    }
}
