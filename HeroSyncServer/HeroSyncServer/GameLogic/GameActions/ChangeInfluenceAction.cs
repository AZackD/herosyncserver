﻿using System;
using System.Collections.Generic;
using System.Text;
using DarkRift;
using HeroSyncCommon;
using HeroSyncCommon.CoreDataTypes;
using HeroSyncServer.GameLogic.EventProcessing;
using HeroSyncServer.GameLogic.EventProcessing.Events;

namespace HeroSyncServer.GameLogic.GameActions
{
    public class ChangeInfluenceAction : GameAction
    {
        public ChangeInfluenceAction(ActionData baseData, GameRoom gr) : base(baseData, gr) { }

        public override void TakeAction(Event e)
        {
            if (e.GetType() != typeof(InfluenceChangeEvent))
            {
                Console.Error.WriteLine("Tried to pass the Change Inf Action an incorrect event type.");
                return;
            }

            InfluenceChangeEvent ice = (InfluenceChangeEvent) e;
            PlayerData affectedPlayer = ice.playerTarget;
            affectedPlayer.influence += ice.amount;
            NetworkStateTags.ServerMessageTags affectedColorTag = affectedPlayer.playerColor == PlayerData.PlayerColor.ORANGE
                ? NetworkStateTags.ServerMessageTags.CHANGE_ORANGE_INFLUENCE
                : NetworkStateTags.ServerMessageTags.CHANGE_BLUE_INFLUENCE;
            gameRoom.SendMessage(affectedPlayer,  affectedColorTag, SendMode.Reliable);
            if (affectedPlayer.influence <= 0)
            {
                gameRoom.PlayerLoss(affectedPlayer);
            }
        }
    }
}
