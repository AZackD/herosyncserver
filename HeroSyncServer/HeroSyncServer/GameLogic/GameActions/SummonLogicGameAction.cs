﻿using System;
using System.Collections.Generic;
using System.Text;
using DarkRift;
using DarkRift.Server;
using HeroSyncCommon;
using HeroSyncCommon.CoreDataTypes;
using HeroSyncCommon.GameLogic;
using HeroSyncCommon.NetworkActions;
using HeroSyncServer.GameLogic.EventProcessing;
using HeroSyncServer.GameLogic.EventProcessing.Events;

namespace HeroSyncServer.GameLogic.GameActions
{
    public class SummonLogicGameAction : GameAction
    {
        public static string SUMMON_TARGET = "TargetCard";
        public static string SUMMON_ZONE = "Zone";

        public CardData cardData;
        public PlayerField.Zone zoneToSummon;
        public Object summoner;

        public override void TakeAction(Event e)
        {
            Type eventType = e.GetType();
            if (eventType != typeof(SummonEvent))
            {
                Console.Error.WriteLine("Trying to use a summon gameAction on the wrong type of event; caller needs to declare a summon-intent event for this gameAction.");
                return;
            }

            SummonEvent sie = (SummonEvent) e;
            cardData = (CardData) sie.GetTarget();
            zoneToSummon = sie.targetedZone;
            summoner = sie.GetSource();
            if (gameRoom == null)
            {
                Console.Error.WriteLine("Summon Actions' game room is null, despite the factory not having a null game room.");
                return;
            }

            gameRoom.AddCardToZone(cardData, zoneToSummon);

            SummonEvent se = (SummonEvent)e;

            MoveCard moveCardMessage = new MoveCard();
            moveCardMessage.cardToMove = se.card;
            moveCardMessage.location = se.targetedZone;
            moveCardMessage.showCardBeforeMoving = true;
            CardPermissionCalculator.SetCardPermissions(cardData, gameRoom.GetCardServerData(cardData));
            gameRoom.SendMessage(moveCardMessage, NetworkStateTags.ServerMessageTags.MOVE_CARD_TO_FIELD, SendMode.Reliable);
        }

        public SummonLogicGameAction(ActionData a, GameRoom g) : base(a, g) { }
    }
}
