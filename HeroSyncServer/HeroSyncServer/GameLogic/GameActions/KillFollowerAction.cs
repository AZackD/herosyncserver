﻿using HeroSyncServer.GameLogic.EventProcessing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using DarkRift;
using HeroSyncCommon;
using HeroSyncCommon.CoreDataTypes;
using HeroSyncCommon.GameLogic;
using HeroSyncCommon.NetworkActions;
using HeroSyncServer.GameLogic.EventProcessing.Events;

namespace HeroSyncServer.GameLogic.GameActions
{
    public class KillFollowerAction : GameAction
    {
        public override void TakeAction(Event e)
        {
            if (e.GetType() != typeof(FollowerKilledEvent))
            {
                Console.WriteLine("Tried to pass a kill-follower action a non-kill follower event {0}, aborting action...", e);
                return;
            }

            FollowerKilledEvent fka = (FollowerKilledEvent) e;
            CardData followerKilled = fka.followerToKill;
            PlayerField.Zone targetZone = fka.zoneToSendOnKilled;

            if (targetZone == PlayerField.Zone.Null)
            {
                PlayerField.Zone blueTargetZone = PlayerField.Zone.Null;
                NetworkStateTags.ServerMessageTags tagForCardMovement = NetworkStateTags.ServerMessageTags.ERASE_CARD;
                switch (fka.sendToOnKilledGenericLocation)
                {
                    case (CardLocation.FallenArea):
                        blueTargetZone = PlayerField.Zone.BlueFallenArea;
                        tagForCardMovement = NetworkStateTags.ServerMessageTags.MOVE_CARD_TO_FALLEN;
                        break;
                    case (CardLocation.Erased):
                        blueTargetZone = PlayerField.Zone.BlueErasedArea;
                        tagForCardMovement = NetworkStateTags.ServerMessageTags.ERASE_CARD;
                        break;
                    case (CardLocation.Deck):
                        blueTargetZone = PlayerField.Zone.BlueDeckArea;
                        tagForCardMovement = NetworkStateTags.ServerMessageTags.MOVE_CARD_TO_DECK;
                        break;
                    default:
                        Console.WriteLine("Using kill follower action with null zone and a {0} location. Possible problems ahead.", fka.sendToOnKilledGenericLocation);
                        break;
                }

                CardServerData csd = gameRoom.GetCardServerData(followerKilled);
                targetZone = (csd.owner.playerColor==PlayerData.PlayerColor.ORANGE) ? ZoneConverter.OpposingZone(blueTargetZone): blueTargetZone;
                gameRoom.AddCardToZone(followerKilled, targetZone);
                MoveCard moveCardMessage = new MoveCard();
                moveCardMessage.cardToMove = followerKilled;
                moveCardMessage.location = targetZone;
                moveCardMessage.showCardBeforeMoving = true;
                gameRoom.SendMessage(moveCardMessage, tagForCardMovement, SendMode.Reliable);
            }

            gameRoom.AddCardToZone(fka.followerToKill, targetZone);
            if (fka.loseInf)
            {
                short infLost = (short) (-fka.followerToKill.level * 10);
                CardServerData csd = gameRoom.GetCardServerData(fka.followerToKill);
                InfluenceChangeEvent ice = new InfluenceChangeEvent(fka, csd.owner, infLost);
                gameRoom.AddEventToStack(ice);
            }
            

        }

        public KillFollowerAction(ActionData a, GameRoom gr) : base(a, gr) { }
    }
}
