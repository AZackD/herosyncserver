﻿using HeroSyncServer.GameLogic.EventProcessing;
using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon;
using HeroSyncCommon.CoreDataTypes;
using HeroSyncServer.GameLogic.EventProcessing.Events;
using HeroSyncServer.GameLogic.EventProcessing.Processors;
using HeroSyncServer.GameLogic.EventProcessing.Processors.EventEvaluators;

namespace HeroSyncServer.GameLogic.GameActions.Factories
{
    class PlayDriveAction : GameAction
    {

        public override void TakeAction(Event e)
        {
            if (e.GetType() != typeof(PlayDriveEvent))
            {
                Console.Error.WriteLine("Tried to use play drive action on a non-drive event typed {0}", e.GetType());
                return;
            }

            PlayDriveEvent pde = (PlayDriveEvent) e;
            CardData cd = pde.playedCard;
            CardServerData driveServerData = gameRoom.cardStatusDictionary[cd];
            foreach (EventProcessor ep in driveServerData.cardEffects)
            {
                if (ep.triggers.Count == 1)
                {
                    EventTriggerEvaluator ete = ep.triggers[0];
                    if (ete.GetType() == typeof(SelfPlayedTriggerEvaluator))
                    {
                        foreach (var driveEvent in ep.actionEvents)
                        {
                            gameRoom.AddEventToStack(driveEvent);
                        }
                    }
                }
            }
        }

        public PlayDriveAction(ActionData a, GameRoom g) : base(a, g) { }
    }
}
