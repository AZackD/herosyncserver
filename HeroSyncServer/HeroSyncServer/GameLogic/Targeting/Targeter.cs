﻿using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncServer.DatabaseHandling;
using HeroSyncServer.GameLogic.EventProcessing.Processors.EventEvaluators;

namespace HeroSyncServer.GameLogic.Targeting
{
    public abstract class Targeter
    {
        List<ObjectCondition> objectConditions = new List<ObjectCondition>();

        public abstract List<Object> GetTargets(GameRoom gr);
        public abstract DBActionRepresentation.TargetAlgorithm GetTargetAlgorithm();

        public void SetTargetConditions(List<ObjectCondition> conditions)
        {
            objectConditions = conditions;
        }

        protected List<Object> PruneOnConditions(List<Object> objectsToPrune)
        {
            List<Object> validObjects = new List<object>();
            foreach (Object obj in objectsToPrune)
            {
                bool meetsCondition = true;
                foreach (ObjectCondition condition in objectConditions)
                {
                    meetsCondition = meetsCondition && condition.MeetsCondition(obj);
                }

                if (meetsCondition)
                {
                    validObjects.Add(obj);
                }
            }
            return validObjects;
        }

    }
}
