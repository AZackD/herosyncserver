﻿using HeroSyncCommon;
using System;
using System.Collections.Generic;
using System.Text;

namespace HeroSyncServer.GameLogic
{
    public static class CardPermissionCalculator
    {
        public static void SetCardPermissions(CardData card, CardServerData status)
        {
            if (card == null)
            {
                Console.Write("Trying to add card permissions to a null card.");
                return;
            }

            card.availableActions = new List<CardActionEnum>();
            switch (card.type)
            {
                case CardType.Follower:
                    AddFollowerPermissions.AddPermissions(card, status);
                    break;
                case CardType.Counter:
                    AddCounterPermissions.AddPermissions(card, status);
                    break;
                case CardType.Drive:
                    AddDrivePermissions.AddPermissions(card, status);
                    break;
                case CardType.Token:
                    AddTokenPermissions.AddPermissions(card, status);
                    break;
                default:
                    Console.Error.Write("Tried to add permissions to a card of an illegal type " + card.type);
                    break; 
            }
        }

        public static void UpdateAllCardPermissions(GameRoom gr)
        {
            foreach (CardData card in gr.cardStatusDictionary.Keys)
            {
                SetCardPermissions(card, gr.cardStatusDictionary[card]);
            }
        }

        public static void ResetAllTransientFlags(GameRoom gameRoom)
        {
            List<Property> transientProperties = CardProperties.GetTransientFlags();
            foreach (CardServerData csd in gameRoom.cardStatusDictionary.Values)
            {
                csd.properties.RemoveAll(x => transientProperties.Contains(x));
            }
        }
    }
}
