﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using HeroSyncCommon.CoreDataTypes;
using HeroSyncServer.DatabaseHandling;
using HeroSyncServer.GameLogic.EventProcessing.Processors.EventEvaluators;
using HeroSyncServer.GameLogic.GameActions;
using HeroSyncServer.GameLogic.Targeting;

namespace HeroSyncServer.GameLogic.EventProcessing.Processors
{
    public class EventProcessorFactory
    {
        private EventEvaluatorTriggerFactory triggerFactory;
        private TargeterFactory targeterFactory;
        private GameRoom gr;

        public EventProcessor CreateCardEventProcessor(List<DBConditionRepresentation> activationConditions, List<DBActionRepresentation> actions)
        {
            EventProcessor eventProcessor = new EventProcessor(gr);
            foreach (DBConditionRepresentation activationCondition in activationConditions)
            {
                EventTriggerEvaluator trigger = triggerFactory.GetTriggerEvaluator(activationCondition);
                Timing triggerTiming = activationCondition.timing;
                eventProcessor.timing = triggerTiming;
                eventProcessor.triggers.Add(trigger);
            }

            //TODO: Sort out how event representations will work with DB representations
            foreach (DBActionRepresentation dbAction in actions)
            {
                Targeter targeter = targeterFactory.GetTargeter(dbAction);

            }
            return eventProcessor;
        }

        public EventProcessorFactory(GameRoom gr)
        {
            if (gr == null)
            {
                throw new NoNullAllowedException("Passed Event Processor Factory a null gameroom. This isn't allowed; the event processors need to know what game to act on.");
            }

            this.gr = gr;
            triggerFactory = new EventEvaluatorTriggerFactory();
            targeterFactory = new TargeterFactory();
        }

    }
}
