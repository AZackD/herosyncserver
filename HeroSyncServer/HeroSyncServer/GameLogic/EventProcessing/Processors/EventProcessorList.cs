﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using HeroSyncCommon;
 using HeroSyncServer.GameLogic.GameActions;
 using HeroSyncServer.GameLogic.Targeting;
 using Org.BouncyCastle.Math.Field;

namespace HeroSyncServer.GameLogic.EventProcessing.Processors
{
    public class EventProcessorList
    {
        private List<EventProcessor> eventProcessors;

        private PlayerData.PlayerColor currentPlayerColor;
        private GameRoom gr;

        private Stack<(GameAction, Event)> afterEffects;
        private Stack<Event> whenEvents;

        private readonly PlayerField.Zone[] bluePlayerLeftToRightZoneOrder = new PlayerField.Zone[]
        {
            PlayerField.Zone.BlueFollowerSlot1,
            PlayerField.Zone.BlueCounterSlot1,
            PlayerField.Zone.BlueFollowerSlot2,
            PlayerField.Zone.BlueCounterSlot2,
            PlayerField.Zone.BlueFollowerSlot3,
            PlayerField.Zone.BlueFallenArea
        };

        private readonly PlayerField.Zone[] orangePlayerLeftToRightZoneOrder = new PlayerField.Zone[]
        {
            PlayerField.Zone.OrangeFollowerSlot1,
            PlayerField.Zone.OrangeCounterSlot1,
            PlayerField.Zone.OrangeFollowerSlot2,
            PlayerField.Zone.OrangeCounterSlot2,
            PlayerField.Zone.OrangeFollowerSlot3,
            PlayerField.Zone.OrangeFallenArea
        };

        public EventProcessorList(GameRoom gr)
        {
            eventProcessors = new List<EventProcessor>();
            this.gr = gr;
            afterEffects = new Stack<(GameAction, Event)>();
            whenEvents = new Stack<Event>();
        }

        /**
         * This function handles when events interrupting other events. It also maintains the list of
         * when-events currently being processed.
         */
        public void ProcessWhenEvent(Event e)
        {
            Console.WriteLine("Checking through {0} processors for event {1}", eventProcessors.Count, e);
            // TODO: Need to sort eventProcessors here
            foreach (var processor in eventProcessors)
            {
                Console.WriteLine("Checking if event {0} is relevant to processor {1}", e, processor);
                if (processor.IsRelevant(e) && processor.timing == Timing.When)
                {
                    whenEvents.Push(e);
                    foreach (var newEventTuple in processor.actionEvents)
                    {
                        Event newEvent = newEventTuple.Item1;
                        Targeter eventTargeter = newEventTuple.Item2;
                        gr.AddEventToStack(newEvent, eventTargeter);
                    } 
                    whenEvents.Pop();
                }
            }
        }

        /**
         * This function handles handling an event after it has finished being called. 
         */
        public Stack<Event> GetNewlyTriggeredAfterEvents(List<Event> events)
        {
            Stack<Event> afterEventsToSendForProcessing = new Stack<Event>();

            // TODO: Sort processors
            foreach (var processor in eventProcessors)
            {
                foreach (Event e in events)
                {
                    if (processor.IsRelevant(e) && processor.timing == Timing.After)
                    {
                        foreach (var activatedAfterEvent in processor.actionEvents)
                        {
                            afterEventsToSendForProcessing.Push(activatedAfterEvent.Item1);
                        }
                    }
                }
            }
            return afterEventsToSendForProcessing;
        }

        public void AddProcessor(EventProcessor newProcessor)
        {
            Console.WriteLine("Adding event processor {0} to list of event processors.");
            if (eventProcessors.Exists(x => x.ID == newProcessor.ID))
            {
                Console.Error.Write("Tried to add a second copy of an event processor that was already present in the list.");
                return;
            }

            eventProcessors.Add(newProcessor);
            // this.RebuildProcessorStack(false);
        }

        public void ChangeCurrentPlayer(PlayerData.PlayerColor color)
        {
            currentPlayerColor = color;
            RebuildProcessorStack(false);
        }

        public List<PlayerField.Zone> buildZoneOrder(bool isReversed)
        {
            List<PlayerField.Zone> result;
            PlayerData.PlayerColor color = currentPlayerColor;
            if (color == PlayerData.PlayerColor.BLUE)
            {
                result = bluePlayerLeftToRightZoneOrder.ToList();
                result.AddRange(orangePlayerLeftToRightZoneOrder.ToList());
            }
            else
            {
                result = orangePlayerLeftToRightZoneOrder.ToList();
                result.AddRange(bluePlayerLeftToRightZoneOrder.ToList());
            }

            if (isReversed)
            {
                result.Reverse();
            }

            return result;
        }

        public void RebuildProcessorStack(bool shouldReverseCardOrder)
        {
            List<EventProcessor> newList = new List<EventProcessor>();
            PlayerField field = gr.gameField;
            List<PlayerField.Zone> mandatoryWhenZoneOrder = buildZoneOrder(shouldReverseCardOrder);
            foreach (PlayerField.Zone zone in mandatoryWhenZoneOrder)
            {
                if (field.cardsInPlay.ContainsKey(zone))
                {
                    CardData card = field.cardsInPlay[zone];
                    CardServerData cardServerData = gr.cardStatusDictionary[card];
                    foreach (EventProcessor ep in cardServerData.cardEffects)
                    {
                        newList.Add(ep);
                    }
                }
            }
        }
    }
}
