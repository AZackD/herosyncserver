﻿using HeroSyncCommon.CoreDataTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace HeroSyncServer.GameLogic.EventProcessing.Processors.EventEvaluators
{
    /**
     * This class is the base event trigger evaluator. It always returns true.
     * All trigger evaluators should decorate this base class. 
     */
    public class DefaultSuccessTriggerEvaluator : EventTriggerEvaluator
    {
        public override bool EvaluateEvent(Event e)
        {
            return true;
        }

        public override ActionData.ActionTypeEnum ReleventTriggerType()
        {
            return ActionData.ActionTypeEnum.NoAction;
        }

        public DefaultSuccessTriggerEvaluator() : base(null, null) { }

    }
}
