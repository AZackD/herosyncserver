﻿using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon.CoreDataTypes;
using HeroSyncServer.DatabaseHandling;
using HeroSyncServer.Utilities;

namespace HeroSyncServer.GameLogic.EventProcessing.Processors.EventEvaluators
{
    public class EventEvaluatorTriggerFactory
    {
        private Dictionary<ActionData.ActionTypeEnum, Type>
            eventTriggerEvaluatorDictionary; 

        public EventTriggerEvaluator GetTriggerEvaluator()
        {
            EventTriggerEvaluator baseEvaluator = new DefaultSuccessTriggerEvaluator();
            return baseEvaluator;
        }

        public EventTriggerEvaluator GetTriggerEvaluator(ActionData.ActionTypeEnum type)
        {
            DBConditionRepresentation dbacr =  new DBConditionRepresentation();
            dbacr.activation_condition_name = type.ToString() + "Default";
            dbacr.timing = Timing.During;
            dbacr.effect_type_id = type;
            dbacr.specifySourceCategories = false;
            dbacr.specifyTargetCategories = false;
            return GetTriggerEvaluator(dbacr);
        }

        public EventTriggerEvaluator GetTriggerEvaluator(DBConditionRepresentation conditionData)
        {
            var type = conditionData.effect_type_id;
            if (!eventTriggerEvaluatorDictionary.ContainsKey(type))
            {
                Console.WriteLine("Tried to build a trigger reactor for a type {0} but the system does not yet have a way to build that type.", type.ToString());
                return null;
            }

            EventTriggerEvaluator baseTrigger = GetTriggerEvaluator();
            EventTriggerEvaluator trigger;

            Object[] inputs = new Object[] {conditionData, baseTrigger};
            trigger = (EventTriggerEvaluator) Activator.CreateInstance(eventTriggerEvaluatorDictionary[type], inputs);
            return trigger;
        }

        public EventTriggerEvaluator GetSelfPlayedTrigger()
        {
            SelfPlayedTriggerEvaluator sptEvaluator = new SelfPlayedTriggerEvaluator();
            return sptEvaluator;
        }

        public EventEvaluatorTriggerFactory()
        {
            eventTriggerEvaluatorDictionary = new Dictionary<ActionData.ActionTypeEnum, Type>();
            var types = GetImplementationUtility.GetImplementations<EventTriggerEvaluator>();
            foreach (var type in types)
            {
                if (!(type.IsInterface || type.IsAbstract) || type.GetGenericParameterConstraints().Length > 0)
                {
                    EventTriggerEvaluator ete = (EventTriggerEvaluator) Activator.CreateInstance(type);
                    if (ete.ReleventTriggerType() != ActionData.ActionTypeEnum.NoAction)
                    {
                        eventTriggerEvaluatorDictionary.Add(ete.ReleventTriggerType(), type);
                    }
                }
            }
        }
    }
}
