﻿using System;
using System.Reflection;
using HeroSyncCommon.CoreDataTypes;

namespace HeroSyncServer.GameLogic.EventProcessing
{
    public abstract class Event
    {
        protected Object source;
        protected Object target;
        protected bool cancelled;

        public Object GetSource()
        {
            return source;
        }

        public void SetTarget(Object target)
        {
            this.target = target;
        }

        public Object GetTarget()
        {
            return target;
        }

        public static Event CopyEvent(Event e)
        {
            Type eventType = e.GetType();
            Object eventObj = Activator.CreateInstance(eventType);
            foreach (FieldInfo field in eventType.GetFields())
            {
                var fieldValue = field.GetValue(e);
                field.SetValue(eventObj, fieldValue);
            }
            return (Event) eventObj;
        }

        public abstract ActionData.ActionTypeEnum GetActionType();

        public Event(Object source)
        {
            this.source = source;
            cancelled = false;
        }

        public void CancelEvent()
        {
            cancelled = true;
        }

        public bool IsCancelled()
        {
            return cancelled;
        }
    }

    public enum Timing
    {
        NotSet = 0,
        When = 1,
        After = 2,
        During = 3,
        NotApplicable = 4
    }

}
