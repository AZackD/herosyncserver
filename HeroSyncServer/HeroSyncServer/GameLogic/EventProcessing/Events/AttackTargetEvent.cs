﻿using HeroSyncCommon.CoreDataTypes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using HeroSyncCommon;
using HeroSyncCommon.NetworkActions;

namespace HeroSyncServer.GameLogic.EventProcessing.Events
{
    public class AttackTargetEvent : Event
    {
        public CardData attacker;
        public CardData targetAsCard;
        public CharacterData targetAsCharacter;
        public FollowerAttackMessage.TargetType targetType;
        public AttackEvent resultingEvent;

        public override ActionData.ActionTypeEnum GetActionType()
        {
            return ActionData.ActionTypeEnum.TargetFollower;
        }

        public AttackTargetEvent(Object source, CardData attacker, CardData target) : base(source)
        {
            this.attacker = attacker;
            this.targetAsCard = target;
            targetType = FollowerAttackMessage.TargetType.CARD;
        }

        public AttackTargetEvent(Object source, CardData attacker, CharacterData target) : base(source)
        {
            this.attacker = attacker;
            this.targetAsCharacter = target;
            targetType = FollowerAttackMessage.TargetType.HERO;
        }
    }
}
