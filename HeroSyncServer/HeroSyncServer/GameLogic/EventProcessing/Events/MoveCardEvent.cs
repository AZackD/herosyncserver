﻿using HeroSyncCommon.CoreDataTypes;
using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon;

namespace HeroSyncServer.GameLogic.EventProcessing.Events
{
    public class MoveCardEvent : Event
    {
        public CardData cardToMove;
        public PlayerField.Zone moveTarget;

        public override ActionData.ActionTypeEnum GetActionType()
        {
            return ActionData.ActionTypeEnum.MoveCard;
        }

        public MoveCardEvent(Object source, CardData card, PlayerField.Zone movingTarget) : base(source)
        {
            cardToMove = card;
            moveTarget = movingTarget;
        }
    }
}
