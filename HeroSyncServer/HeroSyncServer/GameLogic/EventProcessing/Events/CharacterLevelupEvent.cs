﻿using HeroSyncCommon.CoreDataTypes;
using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon;

namespace HeroSyncServer.GameLogic.EventProcessing.Events
{
    public class CharacterLevelupEvent : Event
    {
        private CharacterData character;

        public override ActionData.ActionTypeEnum GetActionType()
        {
            throw new NotImplementedException();
        }

        public CharacterLevelupEvent(Object source, CharacterData character) : base(source) { }
    }
}
