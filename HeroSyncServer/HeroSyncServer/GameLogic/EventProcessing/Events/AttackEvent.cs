﻿using HeroSyncCommon.CoreDataTypes;
using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon;
using HeroSyncCommon.NetworkActions;

namespace HeroSyncServer.GameLogic.EventProcessing.Events
{
    public class AttackEvent : Event
    {
        public AttackTargetEvent instantiatingAte;
        public CardData attackingCard;
        public Object defender;

        public override ActionData.ActionTypeEnum GetActionType()
        {
            return ActionData.ActionTypeEnum.Attack;
        }

        public AttackEvent(Object source, AttackTargetEvent instantiatingATE): base(source)
        {
            this.instantiatingAte = instantiatingATE;
            attackingCard = instantiatingATE.attacker;
            if (instantiatingATE.targetType == FollowerAttackMessage.TargetType.CARD)
            {
                Console.WriteLine("Did the Hero/Card thing not get set properly?");
                defender = instantiatingATE.targetAsCard;
            }
            else
            {
                defender = instantiatingATE.targetAsCharacter;
            }
        }

    }
}
