﻿using HeroSyncCommon.CoreDataTypes;
using System;
using System.Collections.Generic;
using System.Security.Policy;
using System.Text;
using HeroSyncCommon;

namespace HeroSyncServer.GameLogic.EventProcessing.Events
{
    public class FollowerKilledEvent : Event
    {
        public CardData followerToKill;
        public bool loseInf = true;
        public CardLocation sendToOnKilledGenericLocation = CardLocation.FallenArea;
        public PlayerField.Zone zoneToSendOnKilled = PlayerField.Zone.Null;

        public FollowerKilledEvent(Object source, CardData card) : base(source)
        {
            if (card != null && card.type != CardType.Follower && card.type != CardType.Token)
            {
                Console.Error.WriteLine("Tried to build a Follower Killed Event with card \"{0}\" that isn't a follower or token.", card.englishName);
                return;
            }
            followerToKill = card;
            target = card;
        }


        public override ActionData.ActionTypeEnum GetActionType()
        {
            return ActionData.ActionTypeEnum.KillFollower;
        }
    }
}
