﻿using HeroSyncCommon.CoreDataTypes;
using System;
using System.Collections.Generic;
using System.Text;
using HeroSyncCommon;

namespace HeroSyncServer.GameLogic.EventProcessing.Events
{
    public class DrawCardEvent : Event
    {
        public PlayerData drawer;
        public CardData cardToDraw;

        public override ActionData.ActionTypeEnum GetActionType()
        {
            return ActionData.ActionTypeEnum.DrawCard;
        }

        public DrawCardEvent(Object source, PlayerData playerToDraw) : base(source)
        {
            drawer = playerToDraw;
        }

        public DrawCardEvent(Object source, PlayerData playerToDraw, CardData cardToDraw) : base(source)
        {
            drawer = playerToDraw;
            this.cardToDraw = cardToDraw;
        }
    }
}
