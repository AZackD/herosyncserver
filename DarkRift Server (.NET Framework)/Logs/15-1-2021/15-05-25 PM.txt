[Trace]   DarkRiftServer        System Details:
                                	OS: Microsoft Windows NT 6.2.9200.0
                                	CLS Version: 4.0.30319.42000
                                	DarkRift: 2.6.0.0 - Free
[Trace]   pluginFactory         Created plugin 'HeroSyncGameManager'.
[Trace]   PluginManager         Plugin 'HeroSyncGameManager' has requested that DarkRift operates in thread safe mode.
[Trace]   DarkRiftServer        Switched into thread safe mode. Expect lower performance!
[Info]    PluginManager         Loaded plugin HeroSyncGameManager version 0.0.6
[Trace]   pluginFactory         Created plugin 'BichannelListener'.
[Trace]   DefaultNetworkListener Starting bichannel listener.
[Info]    DefaultNetworkListener Server mounted, listening on port 4296.
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:60097.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:61809.
[Info]    ClientManager         New client [0] connected [127.0.0.1:60097|127.0.0.1:61809].
[Info]    HeroSyncGameManager   Writing a log
[Info]    HeroSyncGameManager   Found 15 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    HeroSyncGameManager   Found 15 cards in the database
[Error]   Client                An plugin encountered an error whilst handling the MessageReceived event.
                                 System.ArgumentException: An item with the same key has already been added.
                                    at System.ThrowHelper.ThrowArgumentException(ExceptionResource resource)
                                    at System.Collections.Generic.Dictionary`2.Insert(TKey key, TValue value, Boolean add)
                                    at HeroSyncServer.GameRoom.InitializeDeck(PlayerData player, Int16 deck_id)
                                    at HeroSyncServer.GameRoom.StartSingle()
                                    at HeroSyncServer.GameRoom.CheckForSinglePlayer(Object sender, MessageReceivedEventArgs e)
                                    at DarkRift.Server.Client.<>c__DisplayClass45_0.<HandleIncomingMessage>b__0()
[Info]    ClientManager         Client [0] disconnected.
