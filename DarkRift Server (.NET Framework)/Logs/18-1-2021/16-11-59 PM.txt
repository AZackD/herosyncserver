[Trace]   DarkRiftServer        System Details:
                                	OS: Microsoft Windows NT 6.2.9200.0
                                	CLS Version: 4.0.30319.42000
                                	DarkRift: 2.6.0.0 - Free
[Trace]   pluginFactory         Created plugin 'HeroSyncGameManager'.
[Trace]   PluginManager         Plugin 'HeroSyncGameManager' has requested that DarkRift operates in thread safe mode.
[Trace]   DarkRiftServer        Switched into thread safe mode. Expect lower performance!
[Info]    PluginManager         Loaded plugin HeroSyncGameManager version 0.0.6
[Trace]   pluginFactory         Created plugin 'BichannelListener'.
[Trace]   DefaultNetworkListener Starting bichannel listener.
[Info]    DefaultNetworkListener Server mounted, listening on port 4296.
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:54221.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:50773.
[Info]    ClientManager         New client [0] connected [127.0.0.1:54221|127.0.0.1:50773].
[Info]    HeroSyncGameManager   Writing a log
[Info]    HeroSyncGameManager   Starting single...
[Info]    HeroSyncGameManager   Found 17 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    HeroSyncGameManager   Found 18 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Error]   Client                An plugin encountered an error whilst handling the MessageReceived event.
                                 System.Collections.Generic.KeyNotFoundException: The given key was not present in the dictionary.
                                    at System.ThrowHelper.ThrowKeyNotFoundException()
                                    at System.Collections.Generic.Dictionary`2.get_Item(TKey key)
                                    at HeroSyncServer.SinglePlayerShortcuts.SinglePlayerDebugCommandReceiver.HandleMessage(ClientMessageTags tag, Message m, GameRoom gr)
                                    at HeroSyncServer.SinglePlayerShortcuts.SinglePlayerDebugCommandReceiver.HandleDebugInput(Object sender, MessageReceivedEventArgs e)
                                    at System.EventHandler`1.Invoke(Object sender, TEventArgs e)
                                    at DarkRift.Server.Client.<>c__DisplayClass45_0.<HandleIncomingMessage>b__0()
[Info]    ClientManager         Client [0] disconnected.
