[Trace]   DarkRiftServer        System Details:
                                	OS: Microsoft Windows NT 6.2.9200.0
                                	CLS Version: 4.0.30319.42000
                                	DarkRift: 2.6.0.0 - Free
[Trace]   pluginFactory         Created plugin 'HeroSyncGameManager'.
[Trace]   PluginManager         Plugin 'HeroSyncGameManager' has requested that DarkRift operates in thread safe mode.
[Trace]   DarkRiftServer        Switched into thread safe mode. Expect lower performance!
[Info]    PluginManager         Loaded plugin HeroSyncGameManager version 0.0.7
[Trace]   pluginFactory         Created plugin 'BichannelListener'.
[Trace]   DefaultNetworkListener Starting bichannel listener.
[Info]    DefaultNetworkListener Server mounted, listening on port 4296.
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:59204.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:64211.
[Info]    ClientManager         New client [0] connected [127.0.0.1:59204|127.0.0.1:64211].
[Info]    HeroSyncGameManager   About to start single player room
[Info]    HeroSyncGameManager   Starting single...
[Info]    HeroSyncGameManager   Found 17 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    HeroSyncGameManager   Found 18 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    HeroSyncGameManager   Received message from client. Has tag DEBUG_FORCE_OPP_END_TURN
[Info]    HeroSyncGameManager   Received message from client. Has tag SUMMON_MONSTER
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK_TARGET_REQUEST
[Info]    HeroSyncGameManager   Received message from client. Has tag ATTACK
[Error]   Client                An plugin encountered an error whilst handling the MessageReceived event.
                                 System.NullReferenceException: Object reference not set to an instance of an object.
                                    at HeroSyncServer.GameLogic.GameActions.AttackAction.TakeAction(Event e)
                                    at HeroSyncServer.GameLogic.EventProcessing.Processors.EventStack.AddToStack(Event e)
                                    at HeroSyncServer.GameRoom.AddEventToStack(Event e)
                                    at HeroSyncServer.GameLogic.InputHandlers.AttackTargetHandler.HandleInput(Message m, GameRoom gr)
                                    at HeroSyncServer.GameLogic.InputHandlers.InputHandler.HandleMessage(ClientMessageTags tag, Message m, GameRoom gr)
                                    at HeroSyncServer.GameRoom.HandlePlayerInput(Object sender, MessageReceivedEventArgs e, PlayerData player)
                                    at HeroSyncServer.GameRoom.HandleBluePlayerInput(Object sender, MessageReceivedEventArgs e)
                                    at System.EventHandler`1.Invoke(Object sender, TEventArgs e)
                                    at DarkRift.Server.Client.<>c__DisplayClass45_0.<HandleIncomingMessage>b__0()
[Info]    ClientManager         Client [0] disconnected.
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:64973.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:53144.
[Info]    ClientManager         New client [1] connected [127.0.0.1:64973|127.0.0.1:53144].
