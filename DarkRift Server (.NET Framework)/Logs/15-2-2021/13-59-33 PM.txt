[Trace]   DarkRiftServer        System Details:
                                	OS: Microsoft Windows NT 6.2.9200.0
                                	CLS Version: 4.0.30319.42000
                                	DarkRift: 2.6.0.0 - Free
[Trace]   pluginFactory         Created plugin 'HeroSyncGameManager'.
[Trace]   PluginManager         Plugin 'HeroSyncGameManager' has requested that DarkRift operates in thread safe mode.
[Trace]   DarkRiftServer        Switched into thread safe mode. Expect lower performance!
[Info]    PluginManager         Loaded plugin HeroSyncGameManager version 0.0.7
[Trace]   pluginFactory         Created plugin 'BichannelListener'.
[Trace]   DefaultNetworkListener Starting bichannel listener.
[Info]    DefaultNetworkListener Server mounted, listening on port 4296.
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:49887.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:56298.
[Info]    ClientManager         New client [0] connected [127.0.0.1:49887|127.0.0.1:56298].
[Info]    HeroSyncGameManager   Player attempting to log in.
[Info]    HeroSyncGameManager   Received a message from the client with the CREATE_ROOM tag
[Info]    HeroSyncGameManager   Received a message from the client with the DELETE_ROOM tag
[Info]    HeroSyncGameManager   Received a message from the client with the CREATE_ROOM tag
[Info]    HeroSyncGameManager   Received a message from the client with the JOIN_ROOM tag
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:53206.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:49668.
[Info]    ClientManager         New client [1] connected [127.0.0.1:53206|127.0.0.1:49668].
[Info]    HeroSyncGameManager   Player attempting to log in.
[Info]    HeroSyncGameManager   Received a message from the client with the JOIN_ROOM tag
[Info]    HeroSyncGameManager   Received a message from the client with the SET_READY_TO_START tag
[Info]    HeroSyncGameManager   CHANGING ROOM BECAUSE SOMEONE CLICKED READY
[Info]    HeroSyncGameManager   Received a message from the client with the SET_READY_TO_START tag
[Info]    HeroSyncGameManager   CHANGING ROOM BECAUSE SOMEONE CLICKED READY
[Info]    HeroSyncGameManager   Found 17 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    HeroSyncGameManager   Found 18 cards in the database
[Info]    HeroSyncGameManager   All cards have been given a card status and assigned to a dictionary.
[Info]    ClientManager         Client [1] disconnected.
[Info]    ClientManager         Client [0] disconnected.
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:53213.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:58311.
[Info]    ClientManager         New client [2] connected [127.0.0.1:53213|127.0.0.1:58311].
[Info]    HeroSyncGameManager   Player attempting to log in.
[Info]    HeroSyncGameManager   Received a message from the client with the JOIN_ROOM tag
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:53216.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:58313.
[Info]    ClientManager         New client [3] connected [127.0.0.1:53216|127.0.0.1:58313].
[Info]    HeroSyncGameManager   Player attempting to log in.
[Info]    ClientManager         Client [2] disconnected.
[Info]    HeroSyncGameManager   Received a message from the client with the CREATE_ROOM tag
[Info]    HeroSyncGameManager   Received a message from the client with the JOIN_ROOM tag
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:53219.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:58315.
[Info]    ClientManager         New client [4] connected [127.0.0.1:53219|127.0.0.1:58315].
[Info]    HeroSyncGameManager   Player attempting to log in.
[Info]    ClientManager         Client [3] disconnected.
[Info]    ClientManager         Client [4] disconnected.
[Trace]   DefaultNetworkListener Accepted TCP connection from 127.0.0.1:53222.
[Trace]   DefaultNetworkListener Accepted UDP connection from 127.0.0.1:58317.
[Info]    ClientManager         New client [5] connected [127.0.0.1:53222|127.0.0.1:58317].
[Info]    HeroSyncGameManager   Player attempting to log in.
[Info]    ClientManager         Client [5] disconnected.
